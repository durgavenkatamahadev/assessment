

1.1.1 First repeated character = a
1.1.2 First repeated character = o
1.1.3 No repeated character.
1.1.4 No repeated character.

1.2.1 "aardvark" First repeated character = a
1.2.2 "roommate" First repeated character = o
1.2.3 "mate" java.lang.StringIndexOutOfBoundsException: String index out of range: 4
1.2.4 "test" java.lang.StringIndexOutOfBoundsException: String index out of range: 4

1.3   Yes, there are bugs in `firstRepeatedCharacter` exception is java.lang.StringIndexOutOfBoundsException: String index out of range: 4. to solve this we need to fix in for loop as follows:

		public char firstRepeatedCharacter()
	   {
	      for (int i = 0; i < (word.length())-1; i++)
	      {
	         char ch = word.charAt(i);
	         if (ch == word.charAt(i + 1))
	            return ch;
	      }
	      return 0;
	   }

1.4   If we pass 'null' for string then it will raises null pointer exception. to solve this we need you exception handling with try catch or we check null condition by using if as follows:
			
			public char firstRepeatedCharacter()
		   {
			   if(word!=null)
			   {
			      for (int i = 0; i < (word.length())-1; i++)
			      {
			         char ch = word.charAt(i);
			         if (ch == word.charAt(i + 1))
			            return ch;
			      }
			   }
		      return 0;
		   }
	

2.1.1 First multiple character=i
2.1.2 No multiple character
2.1.3 No multiple character

2.2.1 First multiple character=m
2.2.2 First multiple character=m
2.2.3 First multiple character=t

2.3   yes there are bugs in the code it will not returning output.
      Existing functionality returning always only first chacter wheather its repeated or not

 public char firstMultipleCharacter()
	   {
		   char[] array = word.toCharArray();
		   int count=1;
		   for(int i =1; i < word.length(); i++) {
		       if(array[i] == array[i-1]) {
		           count++;
		           if(count>1){
					   return array[i];   
				   }
		       }
		   }
		  return 0;
	     
	   }

3.1.1  2 repeated characters
3.1.2  0 repeated characters
3.1.3  4 repeated characters

3.2.1 "missisippi" 2 repeated characters
3.2.2 "test" 0 repeated characters
3.2.3 "aabbcdaaaabb" 3 repeated characters

3.3     Yes, there are bugs in the code as it will not returning output correctly so i changed the code as below to get the output.

		public int countRepeatedCharacters()
	   {
	      int c = 0;
	      for (int i = 0; i < word.length() - 1; i++)		// here i have changed i value to 0
	      {
	         if (word.charAt(i) == word.charAt(i + 1)) // found a repetition
	         {
	            if ( i==0 || word.charAt(i - 1) != word.charAt(i)) // here i have added one condition for checking i =0 because other wise it won't count the first group starts at first index itself 
	               c++;
	         }
	      }     
	      return c;
	   }

4.  I have used Breakpoints in  my code to find the bugs.
5.  i)SingleTon design pattern creates only one object that one object will be shared to all the other classes.
    ii)DAO(Data Access Object) design pattern to seperate the persistence logic form business logic.
	iii)DTO(Data Transfer Object) Design pattern to tranfer the data from one layer to another layer.